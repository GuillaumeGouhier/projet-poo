package com.musicmanager;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BaseAudioBooks {
    private List<Audiobook> audiobooks = new ArrayList<>();

    public BaseAudioBooks(List<Audiobook> audiobooks){
        this.audiobooks = audiobooks;
    }

    public BaseAudioBooks(){}

    public List<Audiobook> getAudiobooks(){
        return audiobooks;
    }

    public void setAudiobooks(List<Audiobook> audiobooks){
        this.audiobooks = audiobooks;
    }

    public void addAudioBook(Audiobook toAdd){

        audiobooks.add(toAdd);
        sortByAuthor();
    }

    public void removeAudioBook(Audiobook toDel){
        audiobooks.remove(toDel);
    }

    public Audiobook findByName(String title){
        for(Audiobook a: audiobooks){
            if(a.getTitle().equals(title)){
                return a;
            }
        }
        return null;
    }


    private void sortByAuthor(){

        audiobooks.sort(Comparator.comparing(Audiobook::getAuthor));
    }
}
