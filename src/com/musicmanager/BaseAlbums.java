package com.musicmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BaseAlbums implements Serializable {
    private List<Album> albums = new ArrayList<>();

    public BaseAlbums(List<Album> songs){
        this.albums = songs;
    }

    public BaseAlbums(){}

    public List<Album> getAlbums(){
        return albums;
    }

    public void setAlbums(List<Album> albums){
        this.albums = albums;
    }

    public void addAlbum(Album toAdd){

        albums.add(toAdd);
        sortByDate();
    }

    public void removeAlbum(Album toDel){
        albums.remove(toDel);
    }

    public Album findByName(String title){
        for(Album s: albums){
            if(s.getTitle().equals(title)){
                return s;
            }
        }
        return null;
    }

    public void addSongToAlbum(Integer ID, Song toAdd){
        albums.get(ID).addSong(toAdd);

    }

    private void sortByDate(){
        albums.sort(Comparator.comparing(Album::getDate));
    }
}
