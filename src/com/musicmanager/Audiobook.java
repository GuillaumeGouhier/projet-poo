package com.musicmanager;

public class Audiobook extends Content{


    private String author;
    private Language language;
    private Categorie  Categorie;

    public Audiobook(String author, Language language, Categorie categorie, String titre, Integer duration, Integer ID, String file ) {
        super(titre, duration, ID, file);
        this.author = author;
        this.Categorie = categorie;
        this.language = language;

    }

    public Audiobook(){

    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAuthor() {
        return author;
    }

}
