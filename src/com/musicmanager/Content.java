package com.musicmanager;

import java.io.Serializable;

public abstract class Content implements Serializable{
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getID() {
        return ID;
    }

    public void setID(Integer ID) {
        this.ID = ID;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    protected String title;
    protected Integer duration;
    protected Integer ID;
    protected String file;

    public Content(String titre, Integer duration, Integer ID, String file){
        this.title = titre;
        this.duration = duration;
        this.ID = ID;
        this.file = file;
    }

    public Content(){

    } //serialization need

    @Override
    public boolean equals(Object o){
        return (o instanceof Content) && (title.equals(((Content) o).title));
    }
}
