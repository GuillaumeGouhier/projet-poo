package com.musicmanager;

import java.util.HashSet;
import java.util.Set;

public class Playlist {


    private String name;
    private Integer ID;
    private Set<Content> content;

    // Constructeurs

    public Playlist(){}

    public Playlist(String name, Integer ID){
        this.name = name;
        this.ID = ID;
        content = new HashSet<Content>();
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Set<Content> getContent() {
        return content;
    }

    public void addContent(Content toAdd){
        content.add(toAdd);
    }

    public void removeContent(Content toRemove){
        content.remove(toRemove);
    }

}