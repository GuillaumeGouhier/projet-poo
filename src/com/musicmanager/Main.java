package com.musicmanager;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {

    static BasePlaylists playlists = new BasePlaylists();
    static BaseSongs songs = new BaseSongs();
    static BaseAlbums albums = new BaseAlbums();
    static BaseAudioBooks audioBooks = new BaseAudioBooks();
    static String playlistsFile = "playlists.xml";
    static String albumsFile = "albums.xml";
    static String songsFile = "songs.xml";
    static String audioBooksFile = "audiobooks.xml";
    static Scanner input = new Scanner(System.in);

    /**
     * @param args
     */
    public static void main(String[] args) {

        init();

        while (true) {

            System.out.print("Enter Command:\n");

            switch (input.nextLine().toLowerCase()) {
                case "h" -> help();
                case "c" -> createSong();
                case "a" -> createAlbum();
                case "+" -> addSongToAlbum();
                case "l" -> createAudioBook();
                case "p" -> createPlaylist();
                case "-" -> deletePlaylist();
                case "s" -> serialize();
                case "v" -> printData();
                case "q" -> System.exit(0);
                default -> System.out.println("Commande Inconnue");
            }


        }

    }


    /**
     *
     */
    private static void init (){
        // initialize playlists, albums, chansons

        try {
            XMLDecoder decoder = new XMLDecoder(new FileInputStream(playlistsFile));
            playlists = (BasePlaylists) decoder.readObject();
            decoder.close();


            decoder = new XMLDecoder(new FileInputStream(albumsFile));
            albums = (BaseAlbums) decoder.readObject();
            decoder.close();

            decoder = new XMLDecoder(new FileInputStream(songsFile));
            songs = (BaseSongs) decoder.readObject();
            decoder.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Commands

    static private void help(){
        System.out.print(
                "Press h for help \n" +
                        "Press c to add Song\n" +
                        "Press a to create album\n" +
                        "Press + to add chanson to album\n" +
                        "Press - to delete playlist\n" +
                        "Press l to create audiobook\n" +
                        "Press q to leave\n" +
                        "Press v to view the Data"
        );
        //no exception to manage
    }

    /**
     * @throws IllegalArgumentException
     */
    static private void createSong() throws IllegalArgumentException{

        System.out.println("Entrer  artist");
        String artist = input.nextLine();
        System.out.println("Entrer genre ");
        Genre genre = Genre.valueOf(input.nextLine());
        System.out.println("Entrer titre");
        String title = input.nextLine();
        System.out.println("Entrer durée");
        Integer duration = input.nextInt();
        System.out.println("Entrer le fichier");
        String filePath = input.nextLine();

        if(songs.findByName(title) == null){
            Song toAdd = new Song(artist, genre, title, duration, songs.getSongs().size() + 1, filePath);
            songs.addSong(toAdd);
        }

        else {
            throw new IllegalArgumentException("Song already Exists");
        }



    }

    static void createAudioBook(){
        System.out.println("Entrer  artist");
        String auteur = input.nextLine();
        System.out.println("Entrer Langue ");
        Language language = Language.valueOf(input.nextLine());
        System.out.println("Entrer Categorie");
        Categorie categorie = Categorie.valueOf(input.nextLine());
        System.out.println("Entrer titre");
        String title = input.nextLine();
        System.out.println("Entrer durée");
        Integer duration_ = input.nextInt();
        System.out.println("Entrer le chemin vers le fichier");
        String filePath_ = input.nextLine();

        if(audioBooks.findByName(title) == null) {
            Audiobook audioToAdd = new Audiobook(auteur, language, categorie, title, duration_, audioBooks.getAudiobooks().size() + 1, filePath_);
            audioBooks.addAudioBook(audioToAdd);
        }
        else{
            throw new IllegalArgumentException("AudioBook already Exists");
        }
    }

    static void createAlbum(){
        System.out.println("Entrer titre");
        String title = input.nextLine();
        System.out.println("Entrer Artiste");
        String artist = input.nextLine();
        System.out.println("Entrer Date (format yyyy/mm/dd)");
        String dateString = input.nextLine();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        Date date = new Date();
        try {
            date = dateFormat.parse(dateString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(albums.findByName(title) == null){

        Album albumToAdd = new Album(title, artist, date, albums.getAlbums().size() + 1);
        albums.addAlbum(albumToAdd);
        }
        else {
            throw new IllegalArgumentException("Album Already Exists");
        }
    }

    static void createPlaylist(){
        System.out.print("Enter Playlist name \n");
        String name = input.nextLine();
        //how manage additionnal args?

        if(playlists.findByName(name) == null){
            Playlist playlistToAdd = new Playlist(name, playlists.getPlaylists().size() + 1);
            playlists.addPlaylist(playlistToAdd);
        }
    }

    static void deletePlaylist(){

        System.out.println("Entrer nom playlist");
        String playlistName = input.nextLine();

        playlists.removePlaylist(playlists.findByName(playlistName));
    }

    static private void addSongToAlbum(){

        System.out.println("Entrer nom album");
        String albumName = input.nextLine();
        System.out.println("Entrer nom chanson");
        String songName = input.nextLine();

        if(albums.findByName(albumName) == null){
            throw new IllegalArgumentException("Album does not exists");
        }
        else if(songs.findByName(songName) == null){
            throw new IllegalArgumentException("Song does not exists");
        }

        else {
            albums.addSongToAlbum(albums.findByName(albumName).getID(), songs.findByName(songName));
        }
    }

    static void serialize(){
        XMLEncoder encoder = null;
        try {
            encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(playlistsFile)));
            encoder.writeObject(playlists);
            encoder.close();

            encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(albumsFile)));
            encoder.writeObject(albums);
            encoder.close();

            encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream(songsFile)));
            encoder.writeObject(songs);
            encoder.close();

        } catch (Exception e) {
            System.out.print(e.toString());
        }
    }

    static void printData(){
        System.out.println(playlists.toString());
        System.out.println(albums.toString());
        System.out.println(songs.toString());
        System.out.println(audioBooks.toString());
    }

}

