package com.musicmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BasePlaylists implements Serializable {

    private List<Playlist> playlists = new ArrayList<>();

    public BasePlaylists(List<Playlist> playlists){
        this.playlists = playlists;
    }

    public BasePlaylists(){}

    public List<Playlist> getPlaylists(){
        return playlists;
    }

    public void setPlaylists (List<Playlist>playlists){
        this.playlists = playlists;
    }

    public void addPlaylist(Playlist toAdd){

        playlists.add(toAdd);
        sortPlaylistsByName();
    }

    public void removePlaylist(Playlist toDel){
        playlists.remove(toDel);
    }

    public Playlist findByName(String title){
        for(Playlist p: playlists){
            if(p.getName().equals(title)){
                return p;
            }
        }
        return null;
    }

    private void sortPlaylistsByName() {
        playlists.sort(Comparator.comparing(Playlist::getName));
    }
}
