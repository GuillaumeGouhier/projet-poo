package com.musicmanager;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BaseSongs implements Serializable {
    private List<Song> songs = new ArrayList<>();

    public BaseSongs(List<Song> songs){
        this.songs = songs;
    }

    public BaseSongs(){}

    public List<Song> getSongs(){
        return songs;
    }

    public void setSongs (List<Song>songs){
        this.songs = songs;
    }

    public void addSong(Song toAdd){
        songs.add(toAdd);
    }

    public void removeSong(Song toDel){
        songs.remove(toDel);
    }

    public Song findByName(String title){
        for(Song s: songs){
            if(s.getTitle().equals(title)){
                return s;
            }
        }
        return null;
    }

}
