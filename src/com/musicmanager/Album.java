package com.musicmanager;

import java.io.Serializable;
import java.util.*;

/**
 *
 */
public class Album implements Serializable {
    private String title;
    private String artist;
    private Integer duration;
    private Date date;

    /**
     * @return
     */
    public Integer getID() {
        return ID;
    }

    private Integer ID;
    private List<Song> songs;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Date getDate() {
        return date;
    }

    /**
     * @param date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     *
     */
    public Album(){
    }  // Requis pour la serialisations

    /**
     * @param title
     * @param artist
     * @param date
     * @param ID
     */
    public Album(String title, String artist, Date date, Integer ID){

        this.title = title;
        this.artist = artist;
        this.date = date;
        duration = 0;
        this.ID = ID; // to do
        songs = new ArrayList<Song>();
    }

    public void addSong(Song toAdd){

        songs.add(toAdd);
        duration += toAdd.getDuration();

    }

    /**
     * @param toDel
     */
    public void removeSong(Song toDel){
        songs.remove(toDel);
        duration -= toDel.getDuration();
    }

    /**
     * @return
     */
    public List<Song> getSongs(){
        return songs;
    }

    /**
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o){
        if(o instanceof Album){
            Album toTest = (Album) o;
            return title == toTest.getTitle() && artist == toTest.getArtist() && date == toTest.getDate();
        }

        return false;
    }

    public boolean equals(String title){
        return this.title == title;
    }
}