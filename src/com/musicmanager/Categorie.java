package com.musicmanager;

import java.io.Serializable;

public enum Categorie implements Serializable {

    Jeunesse,
    Roman,
    Theatre,
    Discours,
    Documentaire,

};
