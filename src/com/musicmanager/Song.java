package com.musicmanager;

public class Song extends Content{


    private String artist;
    private Genre genre;

    public com.musicmanager.Genre getGenre() {
        return Genre;
    }

    private Genre Genre;

    public Song(String artist, Genre genre, String titre, Integer duration, Integer ID, String file ) {
        super(titre, duration, ID, file);
        this.artist = artist;
        this.genre = genre;

    }

    public Song(){
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getArtist() {
        return artist;
    }

    public boolean equals(Object o){
        if(o instanceof Song){
            Song toTest = (Song) o;
            return artist == toTest.getArtist() && genre == toTest.getGenre() && getTitle() == toTest.getTitle() && getDuration() == toTest.getDuration() && getFile() == toTest.getFile();
        }
        return false;
    }

    public boolean equals(String title){
        return this.getTitle() == title;
    }

}

