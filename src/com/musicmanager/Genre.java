package com.musicmanager;

import java.io.Serializable;

public enum Genre implements Serializable {
    Jazz,
    Classique,
    HipHop,
    Rock,
    Pop,
    Rap
    
};
